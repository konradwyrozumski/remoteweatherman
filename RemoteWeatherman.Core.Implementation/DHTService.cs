﻿using System;
using Sensors.Dht;
using System.Collections.Generic;
using Windows.UI.Xaml;
using Windows.Devices.Gpio;
using BrewingBeerIoT.Core.Interfaces;
using System.IO;
using System.Runtime.Serialization.Json;

namespace BrewingBeerIoT.Core.Implementation
{
    public class DHTService
    {
        private DispatcherTimer _timer = new DispatcherTimer();

        GpioPin inputPin = null;
        private IDht dht = null;

        private int GpioInputPin = 23;

        DhtReading reading;
        ITimeManagerService timeManagerService;
        IAzureHubService azureHubService;


        public DHTService(ITimeManagerService timeManagerService, IAzureHubService azureHubService)
        {
            inputPin = GpioController.GetDefault().OpenPin(GpioInputPin, GpioSharingMode.Exclusive);
            dht = new Dht22(inputPin, GpioPinDriveMode.Input);
            reading = new DhtReading();

            this.timeManagerService = timeManagerService;
            this.azureHubService = azureHubService;
        }

        public void InitializeReading(int intervalMs)
        {
            Action<object, object> intervalMethod = (object sender, object e) => ReadSensorAndPushToAzure();
            timeManagerService.StartTimer(intervalMs, intervalMethod);
        }

        private async void ReadSensorAndPushToAzure()
        {
            reading = await dht.GetReadingAsync();
            if (reading.IsValid)
            {
                DHTReadingWithTimestamp dhtReadingWithTimestamp = new DHTReadingWithTimestamp(reading);
                string message = Newtonsoft.Json.JsonConvert.SerializeObject(dhtReadingWithTimestamp);
                azureHubService.SendDeviceToCloudMessagesAsync(message);
            }
        }
    }
}
