﻿using System;
using RemoteWeatherman.Core.Interfaces;
using Microsoft.Azure.Devices.Client;
using System.Text;
using RemoteWeatherman.Core.Interfaces.Configuration;

namespace RemoteWeatherman.Core.Implementation
{
    public class AzureHubService : IAzureHubService
    {
        private IAzureConfigurationManager azureConfigurationManager;
        private DeviceClient deviceClient;

        public AzureHubService(IAzureConfigurationManager azureConfigurationManager)
        {
            this.azureConfigurationManager = azureConfigurationManager;
        }


        public async void InitializeAzureHubClient()
        {
            var azureConfiguration = await azureConfigurationManager.ReadAzureConfiguration();

            this.deviceClient = DeviceClient.Create(azureConfiguration.IotHubUri,
                    AuthenticationMethodFactory.
                        CreateAuthenticationWithRegistrySymmetricKey(azureConfiguration.DeviceId, azureConfiguration.DeviceKey),
                    TransportType.Http1);
        }

        public async void SendDeviceToCloudMessagesAsync(object objectToSend)
        {
            string message = Newtonsoft.Json.JsonConvert.SerializeObject(objectToSend);
            var encodedMessage = new Message(Encoding.ASCII.GetBytes(message));
            await deviceClient.SendEventAsync(encodedMessage);
        }
    }
}
