﻿namespace RemoteWeatherman.Core.Interfaces
{
    public interface IAzureHubService
    {
        void InitializeAzureHubClient();
        void SendDeviceToCloudMessagesAsync(object objectToSend);
    }
}
