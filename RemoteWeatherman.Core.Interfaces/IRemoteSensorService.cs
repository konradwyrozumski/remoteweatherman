﻿using RemoteWeatherman.Core.Interfaces.Entities;
using System.Threading.Tasks;

namespace RemoteWeatherman.Core.Interfaces
{
    public interface IRemoteSensorService
    {
        Task<SensorData> GetSensorReading();
        void InitializeSensor();
    }
}
