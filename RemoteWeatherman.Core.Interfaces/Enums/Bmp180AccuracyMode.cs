﻿namespace RemoteWeatherman.Core.Interfaces.Enums
{
    public enum Bmp180AccuracyMode
    {
        UltraLowPower = 0,
        Standard = 1,
        HighResolution = 2,
        UltraHighResolution = 3
    }
}
